import { Component, OnInit } from '@angular/core';
import {MoviePoster} from "../../core/model/movie-poster";
import {MoviesService} from "../../core/provider/movies.service";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  movies: MoviePoster[] = [];

  oppositeLetter = false;
  oppositeRating = false;

  constructor(private moviesService: MoviesService) { }

  ngOnInit(): void {
    this.moviesService.getAllMovies().subscribe(data => {
      this.movies = data.data.movies;
    });
  }

  sortListByLetters() {
    if (this.oppositeLetter) {
      this.movies.sort(function (a, b) {
        if (a.title < b.title) {
          return 1;
        }
        if (a.title > b.title) {
          return -1;
        }
        return 0;
      });
    } else {
      this.movies.sort(function (a, b) {
        if (a.title < b.title) {
          return -1;
        }
        if (a.title > b.title) {
          return 1;
        }
        return 0;
      });
    }
    this.oppositeLetter = !this.oppositeLetter;
  }

  sortListByRatings() {
    if (this.oppositeRating) {
      this.movies.sort(function (a, b) {
        if (a.rating < b.rating) {
          return -1;
        }
        if (a.rating > b.rating) {
          return 1;
        }
        return 0;
      });
    } else {
      this.movies.sort(function (a, b) {
        if (a.rating < b.rating) {
          return 1;
        }
        if (a.rating > b.rating) {
          return -1;
        }
        return 0;
      });
    }
    this.oppositeRating = !this.oppositeRating;
  }

}
