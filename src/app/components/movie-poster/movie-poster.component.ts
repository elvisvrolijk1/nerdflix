import {Component, Input, OnInit} from '@angular/core';
import {MoviePoster} from "../../core/model/movie-poster";

@Component({
  selector: 'app-movie-poster',
  templateUrl: './movie-poster.component.html',
  styleUrls: ['./movie-poster.component.scss']
})
export class MoviePosterComponent implements OnInit {

  @Input() movie!: MoviePoster;

  constructor() { }

  ngOnInit(): void {
  }

  onImageError(event: any) {
    event.target.src = './assets/placeholder.png'
  }
}
