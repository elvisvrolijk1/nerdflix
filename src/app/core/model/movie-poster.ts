export class MoviePoster {
  title: string;
  year: number;
  rating: number;
  urlPoster: string;

  public constructor(tTitle: string, tYear: number, tRating: number, tUrlPoster: string) {
    this.title = tTitle;
    this.year = tYear;
    this.rating = tRating;
    this.urlPoster = tUrlPoster;
  }
}
