import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private Http: HttpClient ) { }

  getAllMovies() : Observable<any> {
    const data = this.Http.get("./assets/imdb-top-50.json");
    return data;
  }
}
